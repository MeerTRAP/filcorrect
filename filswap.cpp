#include <algorithm>
#include <cstdlib>
#include <exception>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

std::vector<unsigned char> make_mask(const std::vector<std::pair<int, int> > &inmask, int nchans) {

    std::vector<unsigned char> tmp(nchans, 1);

    for (auto band : inmask) {

        for (int ichan = band.first; ichan <= band.second; ++ichan) {
            tmp.at(ichan) = 0;
        }

    }

    return tmp;
}

std::vector<unsigned char> make_mask(const std::vector<int> &inmask, int nchans) {

    std::vector<unsigned char> tmp(nchans, 1);

    for (auto chan : inmask) {
        tmp.at(chan) = 0;
    }

    return tmp;
}

int read_header(std::ifstream &infile, char *&header) {

    infile.seekg(0, infile.beg);


    const std::string header_end_str = "HEADER_END";
    int str_len = header_end_str.length();
    int rewind_len = -1 * (str_len - 1);
    int header_len = 0;
    char *read_chars = new char[str_len + 1];

    while(true) {
        infile.read(read_chars, str_len);
        read_chars[str_len] = '\0';
        if (std::string(read_chars) == header_end_str) {
            header_len = infile.tellg();
            std::cout << "Header length: " << header_len << "B\n";
            break;
        }
        infile.seekg(rewind_len, infile.cur);
    }

    infile.seekg(0, infile.beg);
    header = new char[header_len];
    infile.read(reinterpret_cast<char*>(header), header_len);

    return header_len;

}

template <class ParamType>
ParamType read_value(char* header, std::string param) {

    ParamType param_value;

    int param_len = param.length();
    char *head_ptr = header;
    char *read_val = new char[sizeof(ParamType)];
    while(true) {
        if (std::string(head_ptr, head_ptr + param_len) == param) {
            std::copy(head_ptr + param_len, head_ptr + param_len + sizeof(ParamType), read_val);
            param_value = *(reinterpret_cast<ParamType*>(read_val));
            std::cout << "Found parameter " << param << ": " << param_value << "\n";
            break;
        }
        head_ptr++;
    }

    return param_value;

}

void print_help(void) {
    std::cout << "Filterbank correcting code\n";
    std::cout << "Options:\n";
    std::cout << "\t-h - print out this message\n"
                << "\t-f <filename> - filterbank file to process\n"
                << "\t-s - scale the data to 128 mean (unsigned char)\n"
                << "\t-r - swap (rewerse) the ordering of channels in the band\n"
                << "\t-m <filename> - mask channels in the indicated bands\n"
                << "\t--cm <filename> - mask provided channels\n";
}

int main(int argc, char *argv[]) {

    try {

        if (argc == 1) {
            std::cerr << "Incorrect number of arguments provided\n";
            print_help();
            return 1;
        }

        std::string instring;
        std::string outstring;
        
        bool scale = false;
        bool mask = false;
        bool maskb = false;
        bool maskc = false;
        bool swap = false;
        std::string maskstring = "";
        std::vector<std::pair<int, int> > maskbands;
        std::vector<int> maskchans;
        std::vector<unsigned char> fullmask;

        std::ifstream infile; 
        if (!infile) {
            throw std::invalid_argument("cannot open input file " + instring); 
        }
        
        char *header;
        int headlen = 0; 
        int nchans = 0;

        for (int iarg = 1; iarg < argc; ++iarg) {

            if (std::string(argv[iarg]) == "-f") {
                iarg++;
                instring = std::string(argv[iarg]);
                std::cout << "Processing file " << instring << "...\n";
                outstring = instring + ".swapped";
                infile.open(instring.c_str(), std::ios_base::binary);
                headlen = read_header(infile, header);
                nchans = read_value<int>(header, std::string("nchans"));
            } else if (std::string(argv[iarg]) == "-s") {
                std::cout << "Rescaling to 128 mean\n";
                scale = true;
            } else if (std::string(argv[iarg]) == "-m") {
                std::cout << "Masking the channels\n";
                iarg++;
                maskstring = std::string(argv[iarg]);
                mask = true;
                maskb = true;
                std::string maskline;

                std::ifstream inmask(maskstring.c_str());
                if (!inmask) {
                    throw std::invalid_argument("cannot open mask file " + maskstring);
                }

                while (std::getline(inmask, maskline)) {
                    // NOTE: I bet there is a more elegant way of doing it, but not now
                    std::stringstream ssline(maskline);
                    std::string sep;
                    std::vector<int> tmp;
                    while (std::getline(ssline, sep, ',')) {
                        tmp.push_back(std::stoi(sep));
                    }
                    if (tmp.size() == 2) {
                        maskbands.push_back(std::make_pair(tmp.at(0), tmp.at(1)));
                    } else {
                        maskbands.push_back(std::make_pair(tmp.at(0), tmp.at(0)));
                    }

                    tmp.clear();
                }

                inmask.close();

                std::cout << "Bands to mask:\n";
                for (auto band : maskbands) {
                    std::cout << band.first << " " << band.second << "\n";
                }

            } else if (std::string(argv[iarg]) == "--cm") {
                std::cout << "Masking the channels\n";
                iarg++;
                maskstring = std::string(argv[iarg]);
                mask = true;
                maskc = true;
                std::string maskline;

                std::ifstream inmask(maskstring.c_str());
                if (!inmask) {
                    throw std::invalid_argument("cannot open mask file " + maskstring);
                }

                while (std::getline(inmask, maskline)) {
                    maskchans.push_back(std::stoi(maskline));
                }
            
            } else if (std::string(argv[iarg]) == "-r") {
                std::cout << "Will swap channels in the band\n";
                swap = true; 
            } else if (std::string(argv[iarg]) == "-h") {
                print_help();
                return 0;
            } else {
                std::cout << "Unrecognised option: " << std::string(argv[iarg]) << std::endl;
                print_help();
                return 1;
            }

        }


        if (maskb) {
            fullmask = make_mask(maskbands, nchans);
        } else if (maskc) {
            fullmask = make_mask(maskchans, nchans);
        }

        if (mask) {
            std::cout << "Will mask " << std::count(fullmask.begin(), fullmask.end(), 0) << " channels\n";
        }

        infile.seekg(0, infile.end);

        size_t samples = (static_cast<size_t>(infile.tellg()) - static_cast<size_t>(headlen)) / static_cast<size_t>(nchans);

        if ((samples * nchans) != (static_cast<size_t>(infile.tellg()) - static_cast<size_t>(headlen))) {
            throw std::logic_error("did not read an integer number of samples");
        }

        std::cout << "Read " << samples << " time samples\n";

        infile.seekg(0, infile.beg);
        infile.read(reinterpret_cast<char*>(header), headlen * sizeof(unsigned char));

        infile.seekg(headlen, infile.beg);

        std::ofstream outfile(outstring.c_str(), std::ios_base::binary);
        if (!outfile) {
            throw std::runtime_error("cannot open the output file " + outstring);
        }

        outfile.write(reinterpret_cast<char*>(header), headlen * sizeof(char));

        std::vector<char> band(nchans, 0);

        std::cout << std::fixed << std::setprecision(2);

        for (size_t isamp = 0; isamp < samples; ++isamp) {
            infile.read(&band[0], nchans * sizeof(unsigned char));
            // NOTE: This assumes mask is pre-swap mask if swap is applied
            // and correct order if no swap is applied
            if (mask) {
                std::transform(band.begin(), band.end(), fullmask.begin(), band.begin(), std::multiplies<unsigned char>());
            }
            if (swap) {
                std::reverse(band.begin(), band.end());
            }
            if (scale) {
                std::for_each(band.begin(), band.end(), [](char &val){val += 128;});
            }


            outfile.write(&band[0], nchans * sizeof(unsigned char));
            std::cout << "Processed " << static_cast<float>(isamp + 1) / samples * 100.0f << "%\r";
            std::cout << std::flush;
        }      

        outfile.close();
        infile.close();

    } catch (const std::exception &exc) {
        std::cerr << "Caught exception: " << exc.what() << std::endl;
    }
}
